const TelegramBot = require('node-telegram-bot-api');
const SMTPServer = require('smtp-server').SMTPServer;
const config = require('./config');
const lPort = 2525;
var bot = new TelegramBot(config.botToken, {
  polling: false
});
msg = new Buffer([]);

/* You can enable this to find out which chatId you or your group are/is
 * =====================================================================
 */
// var bot = new TelegramBot(config.botToken, {polling: true});
// bot.on('message', (msg) => {
//   const chatId = msg.chat.id;
//   bot.sendMessage(chatId, 'Received your message. Your are chatId: ' + chatId);
// });

const server = new SMTPServer({
  secure: false,
  banner: "Welcome to SMTP-Telegram gateway!",
  authOptional: true,
  disableReverseLookup: true,

  onConnect(session, callback) {
    /* Enable this to only allow localhost connections
     * =====================================================================
     */
    // if (session.remoteAddress !== '127.0.0.1' && session.remoteAddress !== '::1') {
    //   return callback(new Error('Only connections from localhost allowed'));
    // }
    log("New Client from " + session.remoteAddress);
    // Clear buffer for new client
    msg = new Buffer([]);
    return callback(); // Accept the connection
  },

  onData(stream, session, callback) {
    log("Data incoming from " + session.remoteAddress);
    stream.on('data', newData);
    stream.on('end', function() {
      callback();
    });
  },

  onClose(session) {
    log("Client closed connection from " + session.remoteAddress);
    if (msg.length > 0) {
      sendData();
    }
  },

});


function newData(data) {
  msg = Buffer.concat([msg, data]);
}

function sendData() {
  log("Message will be delivered now..");
  var startBody = (msg.indexOf("<body>") == -1) ? msg.indexOf("<body ") : msg.indexOf("<body>");
  var endBody = msg.indexOf("</body>") + 7; // 7 because we want the pattern to be included
  var newLength = msg.length - startBody - (msg.length - endBody);
  log("Extracted " + newLength + " bytes of body content from overall " + msg.length + " bytes");
  var body = msg.slice(startBody, endBody);

  if (body.indexOf("SMTP Settings Test") >= 0) {
    // This email is only a test. Say that it
    // was recieved and we are ready for the alerts
    // to come
    messageForBot =
    `✅ <b>UniFi Telegram Gateway</b>
    Your UniFi Telegram Gateway is working!
     - Awesome!`;

  } else if (msg.indexOf("Subject: [UniFi] Alert: ") >= 0) {
    // This is a pattern match of the Email's subject
    var alarmType = getContentBetween(body, "UniFi Alarm: ", "</p>");
    var device = getContentBetween(body, "Device name: ", "</p>");
    var site = getContentBetween(body, "Site: ", "</p>"); // We don't use this, but you can add it below if you want
    var message = getContentBetween(body, "Message: ", "</p>");
    var controllerUrl = getContentBetween(body, "Controller URL: <a href=\"", "\"");

    messageForBot =
      `⁉️ <b>UniFi Alert: ${alarmType}</b>
      Device: ${device}
      Issue: <b>${message}</b>
      <a href="${controllerUrl}">open Controller</a>
      `;

  } else {
    messageForBot =
    `❔ <b>UniFi Telegram Gateway</b>
    Ouch, this shouldn't happen! The type of Mail was not recognized.
    Maybe a controller update broke it?`;
  }


  log(messageForBot);
  bot.sendMessage(config.tecGroupIdCritical, messageForBot, {
    parse_mode: "HTML"
  });
  log("Message is out");
}

function getContentBetween(buf, start, end) {
  var start = buf.indexOf(start) + start.length;
  var end = buf.indexOf(end, start);
  if (start == -1 || end == -1) {
    return new Buffer([]);
  }
  return buf.slice(start, end);
}

function log(data) {
  console.log(getDateTime() + ": " + data);
}

function getDateTime() {
  var date = new Date();
  var hour = date.getHours();
  hour = (hour < 10 ? "0" : "") + hour;
  var min = date.getMinutes();
  min = (min < 10 ? "0" : "") + min;
  var sec = date.getSeconds();
  sec = (sec < 10 ? "0" : "") + sec;
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  month = (month < 10 ? "0" : "") + month;
  var day = date.getDate();
  day = (day < 10 ? "0" : "") + day;
  var msec = date.getMilliseconds();
  msec = (msec < 10 ? "00" : (msec < 100 ? "0" : "")) + msec;
  return day + "." + month + " " + hour + ":" + min + ":" + sec + ":" + msec;
}


log("Server starts listening on port " + lPort);
server.listen(lPort);
