# Unifi Telegram Bridge

I missed a way to push events from the UniFi Controller to our Telegram group. Waiting for a mail to be delivered is not really realtime. So, with this you get an SMTP Server which will forward any mail (which matches some patterns) to a Telegram group defined in `config.js`

# Compatibility
Working with Controller version **5.7.23**

# Files

- `config.js.sample`
	- Edit with your token, groupId and rename to **`config.js`**
- `package.json`
	- Install the libs with **npm install**
- `server.js`
	- start with **node server.js**

## Setup UniFi Controller
Go to `Settings` -> `Controller` -> `MAIL SERVER`

- `Hostname`: 127.0.0.1
- `Port`: 2525
- `No SSL, no authentication`
- `Set any sender address`

If you have the server up and running, you can send a Test Email and should see a confirmation in the group.

## How to have it run at boot?
Get pm2. Simply great. [pm2.keymetrics.io](http://pm2.keymetrics.io/)

Then start with:

- pm2 start --name telegram-bridge server.js

Save the startup config with

- `pm2 startup`
- `pm2 save` 
